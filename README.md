# stretch2x_cmake 

Related to the master's thesis "Heterogeneous hw / sw acceleration of a video game in a reconfigurable MPSoC" carried out at the UPM University in 2018. This is a cmake project that isolates the "stretch2x" function from the Crispy-DOOM source code. Its functionality is to read an input 640x400 frame and create an output 1280x960 frame.  

## Getting Started

Compile using cmake, a bash script is given to do so.

```
source cmake.sh
```

 A doom_test file is created under build/ directory. Execute it to run the test. 
 
```
./doom_test
```


## Authors

* **David Lima** - *davidlimaastor@gmail.com* - [BitBucket](https://bitbucket.org/d_lima)

## License

No license, feel free to use as an example.

