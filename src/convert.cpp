#include "convert.h"

byte* matToBytes(cv::Mat image)
{
   int size = image.total() * image.channels();
   byte * bytes = new byte[size];  // you will have to delete[] that later
   std::memcpy(bytes,image.data,size * sizeof(byte));
}

cv::Mat bytesToMat(byte * bytes,int width,int height)
{
    cv::Mat image = cv::Mat(height, width, CV_8UC3, bytes).clone(); // make a copy
    return image;
}
