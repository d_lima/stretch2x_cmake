#ifndef MAINHEADER_H
#define MAINHEADER_H

#include <opencv2/opencv.hpp>

#include "doomtype.h"
#include <stddef.h>
#include <stdio.h>

#include "I_Stretch2x_Function.h"
#include "convert.h"

typedef unsigned char byte;

#define X1 (0)
#define X2 (640)
#define Y1 (0)
#define Y2 (400)

#define INPUT_WIDTH  (640)
#define INPUT_HEIGHT (400)

#define OUTPUT_WIDTH  (1280)
#define OUTPUT_HEIGHT (960)


// Pitch of destination buffer, ie. screen->pitch.
#define PITCH (1280)


#endif // MAINHEADER_H
