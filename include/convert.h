#ifndef CONVERT_H
#define CONVERT_H

#include "mainheader.h"


byte * matToBytes(cv::Mat image);

cv::Mat bytesToMat(byte * bytes,int width,int height);


#endif // CONVERT_H
